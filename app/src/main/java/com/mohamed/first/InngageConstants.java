package com.mohamed.first;

public interface InngageConstants {

    String inngageAppToken = "e1d94098207573a0eaf1edb90aaeaa5b"; //application token retirado da plataforma Inngage
    String inngageEnvironment = "prod";
    String googleMessageProvider = "FCM";

    int updateInterval = 60000;
    int priorityAccuracy = 104;
    int displacement = 100;
    int PERMISSION_ACCESS_LOCATION_CODE = 99;
    String LOCATION_NOT_FOUND = "Não foi possível obter a localização do usuário";
}
