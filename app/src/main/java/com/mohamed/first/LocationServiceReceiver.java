package com.mohamed.first;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import br.com.inngage.sdk.InngageServiceUtils;

public class LocationServiceReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {


        InngageServiceUtils serviceUtils = new InngageServiceUtils(context);
        serviceUtils.startService(
                InngageConstants.updateInterval,
                InngageConstants.priorityAccuracy,
                InngageConstants.displacement,
                InngageConstants.inngageAppToken);
    }
}
