package com.mohamed.first

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import br.com.inngage.sdk.GrantPermission;
import br.com.inngage.sdk.InngageIntentService;
import br.com.inngage.sdk.InngagePermissionUtil;
import br.com.inngage.sdk.InngageServiceUtils;
import br.com.inngage.sdk.InngageUtils;

//possivelmente você também irá precisar das seguintes importações também
import android.Manifest;
import android.content.Intent
import android.content.pm.PackageManager;
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Log.d
import android.widget.Toast
import br.com.inngage.sdk.InngageServiceUtils.startService
import br.com.inngage.sdk.InngageServiceUtils.startService
import org.json.JSONException
import org.json.JSONObject
import kotlin.math.log


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class MainActivity : AppCompatActivity() {
    var serviceUtils = InngageServiceUtils(this@MainActivity)
    var mBothPermissionRequest: InngagePermissionUtil.PermissionRequestObject? = null
    private val TAG = "INNGAGE"
    var id: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)




        handleSubscription();
        handleNotification();
        handleLocation();
        askPermissions();

    }


    private fun handleSubscription() {

//        val jsonCustomField = JSONObject()
//        try
//        {
//            jsonCustomField.put("nome", "")
//            jsonCustomField.put("email", "")
//            jsonCustomField.put("telefone", "")
//            jsonCustomField.put("dataRegistro", "")
//            jsonCustomField.put("dataNascimento", "")
//        }
//        catch (e: JSONException) {
//            e.printStackTrace()
//        }
//        InngageIntentService.startInit(
//            this,
//            InngageConstants.inngageAppToken,
//            "Identifier", //Seu identificador
//            InngageConstants.inngageEnvironment,
//            InngageConstants.googleMessageProvider,
//            jsonCustomField)

        InngageIntentService.startInit(
            this,
            InngageConstants.inngageAppToken,
            "example", //Seu identificador
            InngageConstants.inngageEnvironment,
            InngageConstants.googleMessageProvider
        )
    }


    private fun handleNotification() {

        var notifyID: String? = ""
        var title: String? = ""
        var body: String? = ""
        var url: String? = ""

        val bundle = intent.extras

        if (intent.hasExtra("EXTRA_NOTIFICATION_ID")) {

            notifyID = bundle!!.getString("EXTRA_NOTIFICATION_ID")
        }
        if (intent.hasExtra("EXTRA_TITLE")) {

            title = bundle!!.getString("EXTRA_TITLE")
        }
        if (intent.hasExtra("EXTRA_BODY")) {

            body = bundle!!.getString("EXTRA_BODY")
        }

        if (intent.hasExtra("EXTRA_URL")) {

            url = bundle!!.getString("EXTRA_URL")
        }

        if (url!!.isEmpty()) {
            if ("" != notifyID || "" != title || "" != body) {
                d(TAG, "no link: $url")
                InngageUtils.showDialog(
                    title,
                    body,
                    notifyID,
                    InngageConstants.inngageAppToken,
                    InngageConstants.inngageEnvironment,
                    this
                )
            }

        } else if ("" != notifyID || "" != title || "" != body) {
            d(TAG, "Link: $url")
            InngageUtils.showDialogwithLink(
                title,
                body,
                notifyID,
                InngageConstants.inngageAppToken,
                InngageConstants.inngageEnvironment,
                url,
                this
            )
        }

    }


    fun askPermissions() {
        mBothPermissionRequest = InngagePermissionUtil.with(this).request(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        ).onResult(
            object : GrantPermission() {
                override fun call(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                        serviceUtils.startService(
                            InngageConstants.updateInterval,
                            InngageConstants.priorityAccuracy,
                            InngageConstants.displacement,
                            InngageConstants.inngageAppToken
                        )

                    } else {

                        Toast.makeText(this@MainActivity, InngageConstants.LOCATION_NOT_FOUND, Toast.LENGTH_LONG).show()
                    }
                }

            }).ask(InngageConstants.PERMISSION_ACCESS_LOCATION_CODE)
    }


    private fun handleLocation() {

        if (InngageUtils.hasM() && !(ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED)
        ) {

            askPermissions()

        } else {

            serviceUtils.startService(
                InngageConstants.updateInterval,
                InngageConstants.priorityAccuracy,
                InngageConstants.displacement,
                InngageConstants.inngageAppToken
            )
        }
    }
}

private fun InngageServiceUtils.startService(updateInterval: Int, priorityAccuracy: Int, displacement: Int, inngageAppToken: String) {

}
